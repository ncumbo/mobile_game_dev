﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiHandler : MonoBehaviour {

	public Text txtSeeds;
	public int seeds;
	public Text txtWater;
	public int water;
	public Text txtPumpkins;
	public int pumpkins;
	public Text txtMana;
	public int mana;
	RPGGameContractCaller contractCaller;
    float StartTime = 60;


	// Use this for initialization
	void Start () {
		contractCaller = GameObject.Find("EventHandler").GetComponent<RPGGameContractCaller>();

		seeds = 0;
		water = 0;
		pumpkins = 0;
		mana = 0;
		
		contractCaller.getManaForAddress();
		contractCaller.getSeedsForAddress();
		contractCaller.getPumpkinsForAddress();

	}
	
	// Update is called once per frame
	void Update () {
		// StartTime -= Time.deltaTime;
        // // txtTimer.text = Mathf.Round(StartTime).ToString();
        // if (StartTime < 0)
        // {
		// 	contractCaller.getManaForAddress();
		// 	contractCaller.getSeedsForAddress();
		// 	// contractCaller.getPumpkinsForAddress();
        //     Debug.Log ("getBalance");
        //     StartTime = 60;
        // }


		txtPumpkins.text = "Pumpkins "+pumpkins;
		txtSeeds.text = "Seeds "+seeds;
		txtWater.text = "Water "+water;
		txtMana.text = "Mana "+mana;
	}


}
