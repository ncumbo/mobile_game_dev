﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Nethereum.JsonRpc.UnityClient;
using Nethereum.Hex.HexTypes;
using Nethereum.Contracts;

public class RPGGameContractCaller : MonoBehaviour {

    private DeployContractTransactionBuilder contractTransactionBuilder = new DeployContractTransactionBuilder();

    private RPGGameContractService rpgGameContractService = new RPGGameContractService();
                        
	UiHandler uiEventHandler;

    //---------------------------------------------------------------------------------MANA
    #region MANA
    public void buyMana(){
        StartCoroutine(buyManaRequest());
    }

    public IEnumerator buyManaRequest(){
        //this delay can be deleted. only done since we are going to call from start
        // yield return new WaitForSeconds(3);
        var transactionInput = rpgGameContractService.BuyManaTransactionInput(new HexBigInteger(3000000), new HexBigInteger(250000000000000000));

        var transactionSignedRequest = new TransactionSignedUnityRequest(LevelManager._url,LevelManager.objAddress.PrivateKey,LevelManager.objAddress.Address);

        //send and wait (run smart contract)
        yield return transactionSignedRequest.SignAndSendTransaction(transactionInput);
        if(transactionSignedRequest.Exception == null){
            print("Transaction receipt:" + transactionSignedRequest.Result);//transaction address

            while(true){ //every 3 seconds check for transaction feedback until received
                yield return new WaitForSeconds(3);
                EthGetTransactionReceiptUnityRequest tranReceiptStatus =
                    new EthGetTransactionReceiptUnityRequest(LevelManager._url);
                yield return tranReceiptStatus.SendRequest(transactionSignedRequest.Result);
                if(tranReceiptStatus.Result != null){
                    if(tranReceiptStatus.Result.Status != null){
                        if(tranReceiptStatus.Result.Status.Value == 1){//smart contract was successfully executed
                            print("Purchased Mana Successfully");
                            getManaForAddress();
                        }
                        else if(tranReceiptStatus.Result.Status.Value == 0){
                            print("Error when buying mana");
                        }
                        break;
                    }
                }

            }

        }
        else{
            print("Error when buying mana:" + transactionSignedRequest.Exception.Message);
        }

    }

    public void getManaForAddress(){
        StartCoroutine(getManaRequestForAddress());
    }

    IEnumerator getManaRequestForAddress(){
        yield return new WaitForSeconds(3);
        var getManaRequest = new EthCallUnityRequest(LevelManager._url);
        var getManaInput = rpgGameContractService.CreateManaInput();
        print("Loading mana for account:" + LevelManager.objAddress.Address);
        yield return getManaRequest.SendRequest(getManaInput,Nethereum.RPC.Eth.DTOs.BlockParameter.CreateLatest());
        if(getManaRequest.Exception == null){
            int mana = rpgGameContractService.DecodeManaValue(getManaRequest.Result);
            uiEventHandler.mana = mana;
            print("Mana:" + mana);
        }
        else{
            print("Error getting mana:" + getManaRequest.Exception.Message);
        }

    }

    #endregion

    //---------------------------------------------------------------------------------SEEDS
    #region SEEDS
    public void buySeeds(){
        StartCoroutine(buySeedsRequest());
    }

    public IEnumerator buySeedsRequest(){
        //this delay can be deleted. only done since we are going to call from start
        // yield return new WaitForSeconds(3);
        var transactionInput = rpgGameContractService.BuySeedsTransactionInput(new HexBigInteger(3000000), new HexBigInteger(250000000000000000));

        var transactionSignedRequest = new TransactionSignedUnityRequest(LevelManager._url,LevelManager.objAddress.PrivateKey,LevelManager.objAddress.Address);

        //send and wait (run smart contract)
        yield return transactionSignedRequest.SignAndSendTransaction(transactionInput);
        if(transactionSignedRequest.Exception == null){
            print("Transaction receipt:" + transactionSignedRequest.Result);//transaction address

            while(true){ //every 3 seconds check for transaction feedback until received
                yield return new WaitForSeconds(3);
                EthGetTransactionReceiptUnityRequest tranReceiptStatus =new EthGetTransactionReceiptUnityRequest(LevelManager._url);
                yield return tranReceiptStatus.SendRequest(transactionSignedRequest.Result);
                if(tranReceiptStatus.Result != null){
                    if(tranReceiptStatus.Result.Status != null){
                        if(tranReceiptStatus.Result.Status.Value == 1){//smart contract was successfully executed
                            print("Purchased Seeds Successfully");
                            getSeedsForAddress();
                        }
                        else if(tranReceiptStatus.Result.Status.Value == 0){
                            print("Error when buying seeds");
                        }
                        break;
                    }
                }

            }

        }
        else{
            print("Error when buying seeds:" + transactionSignedRequest.Exception.Message);
        }

    }

    public void getSeedsForAddress(){
        StartCoroutine(getSeedsRequestForAddress());
    }

    IEnumerator getSeedsRequestForAddress(){
        yield return new WaitForSeconds(3);
        var getSeedsRequest = new EthCallUnityRequest(LevelManager._url);
        var getSeedsInput = rpgGameContractService.CreateSeedsInput();
        print("Loading seeds for account:" + LevelManager.objAddress.Address);
        yield return getSeedsRequest.SendRequest(getSeedsInput,Nethereum.RPC.Eth.DTOs.BlockParameter.CreateLatest());
        if(getSeedsRequest.Exception == null){
            int seeds = rpgGameContractService.DecodeSeedsValue(getSeedsRequest.Result);
            uiEventHandler.seeds = seeds;
            print("Seeds:" + seeds);
        }
        else{
            print("Error getting seeds:" + getSeedsRequest.Exception.Message);
        }

    }
    
    #endregion
    //-----------------------------------------------------------------------------------MANA & SEEDS UPDATE
    public void updateManaAndSeedsForAddress(){
        StartCoroutine(updateManaAndSeedsRequest());
    }

    public IEnumerator updateManaAndSeedsRequest(){
        //this delay can be deleted. only done since we are going to call from start
        // yield return new WaitForSeconds(3);
        var transactionInput = rpgGameContractService.UpdateManaAndSeedsTransactionInput(uiEventHandler.seeds, uiEventHandler.mana, new HexBigInteger(3000000), new HexBigInteger(0));

        var transactionSignedRequest = new TransactionSignedUnityRequest(LevelManager._url,LevelManager.objAddress.PrivateKey,LevelManager.objAddress.Address);

        //send and wait (run smart contract)
        yield return transactionSignedRequest.SignAndSendTransaction(transactionInput);
        if(transactionSignedRequest.Exception == null){
            print("Transaction receipt:" + transactionSignedRequest.Result);//transaction address

            while(true){ //every 3 seconds check for transaction feedback until received
                yield return new WaitForSeconds(3);
                EthGetTransactionReceiptUnityRequest tranReceiptStatus = new EthGetTransactionReceiptUnityRequest(LevelManager._url);
                yield return tranReceiptStatus.SendRequest(transactionSignedRequest.Result);
                if(tranReceiptStatus.Result != null){
                    if(tranReceiptStatus.Result.Status != null){
                        if(tranReceiptStatus.Result.Status.Value == 1){//smart contract was successfully executed
                            print("Updated Mana And Seeds Successfully");
                        }
                        else if(tranReceiptStatus.Result.Status.Value == 0){
                            print("Error when updating Mana And Seeds");
                        }
                        break;
                    }
                }

            }

        }
        else{
            print("Error when updating Mana And Seeds:" + transactionSignedRequest.Exception.Message);
        }

    }

    //---------------------------------------------------------------------------------PUMPKINS
    #region PUMPKINS
    public void addPumpkins(){
        StartCoroutine(addPumpkinsRequest());
    }

    public IEnumerator addPumpkinsRequest(){
        //this delay can be deleted. only done since we are going to call from start
        // yield return new WaitForSeconds(3);
        var transactionInput = rpgGameContractService.AddPumpkinsTransactionInput(uiEventHandler.pumpkins, new HexBigInteger(3000000), new HexBigInteger(0));

        var transactionSignedRequest = new TransactionSignedUnityRequest(LevelManager._url,LevelManager.objAddress.PrivateKey,LevelManager.objAddress.Address);

        //send and wait (run smart contract)
        yield return transactionSignedRequest.SignAndSendTransaction(transactionInput);
        if(transactionSignedRequest.Exception == null){
            print("Transaction receipt:" + transactionSignedRequest.Result);//transaction address

            while(true){ //every 3 seconds check for transaction feedback until received
                yield return new WaitForSeconds(3);
                EthGetTransactionReceiptUnityRequest tranReceiptStatus =new EthGetTransactionReceiptUnityRequest(LevelManager._url);
                yield return tranReceiptStatus.SendRequest(transactionSignedRequest.Result);
                if(tranReceiptStatus.Result != null){
                    if(tranReceiptStatus.Result.Status != null){
                        if(tranReceiptStatus.Result.Status.Value == 1){//smart contract was successfully executed
                            print("Pumpkins Added Successfully");
                            getPumpkinsForAddress();
                        }
                        else if(tranReceiptStatus.Result.Status.Value == 0){
                            print("Error when adding pumpkins");
                        }
                        break;
                    }
                }

            }

        }
        else{
            print("Error when buying pumpkins:" + transactionSignedRequest.Exception.Message);
        }
    }

    public void getPumpkinsForAddress(){
        StartCoroutine(getPumpkinsRequestForAddress());
    }

    IEnumerator getPumpkinsRequestForAddress(){
        yield return new WaitForSeconds(3);
        var getPumpkinsRequest = new EthCallUnityRequest(LevelManager._url);
        var getPumpkinsInput = rpgGameContractService.CreatePumpkinsInput();
        print("Loading pumpkins for account:" + LevelManager.objAddress.Address);
        yield return getPumpkinsRequest.SendRequest(getPumpkinsInput,Nethereum.RPC.Eth.DTOs.BlockParameter.CreateLatest());
        if(getPumpkinsRequest.Exception == null){
            int pumpkins = rpgGameContractService.DecodePumpkinsValue(getPumpkinsRequest.Result);
            uiEventHandler.pumpkins = pumpkins;
            print("Pumpkins:" + pumpkins);
        }
        else{
            print("Error getting pumpkins:" + getPumpkinsRequest.Exception.Message);
        }

    }
    #endregion
    //---------------------------------------------------------------------------------

	// Use this for initialization
	void Start () {
        uiEventHandler = GameObject.Find("EventHandler").GetComponent<UiHandler>();
        //toggle between these two methods for testing
        //getManaForAddress();
        // Debug.Log("-------------------------------------------------------");
        // buyMana();
        // Debug.Log("-------------------------------------------------------");
        // getManaForAddress();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
