﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Nethereum.JsonRpc.UnityClient;

public class BalanceCheck : MonoBehaviour {

    public int Balance;
    public Text txtEther;
    float StartTime = 60;
	// Use this for initialization
	void Start () {
        txtEther.text = "Ether: 0";
        getBalance();
	}
	
	// Update is called once per frame
	void Update () {
        
        StartTime -= Time.deltaTime;
        // txtTimer.text = Mathf.Round(StartTime).ToString();
        if (StartTime < 0)
        {
            getBalance();
            Debug.Log ("getBalance");
            StartTime = 60;
        }

	}
    private void getBalance(){
        StartCoroutine(getEthInAccount((balance) =>
        {
		    Balance = (int)balance;
            print("Balance in account:" + balance);
            txtEther.text = "Ether "+balance;
        }));
    }

    public IEnumerator getEthInAccount(System.Action<decimal> callback){
        yield return new WaitForSeconds(3); //this delay is done so that we give time for wallet to load
        var request = new EthGetBalanceUnityRequest(LevelManager._url);
        yield return request.SendRequest(LevelManager.objAddress.Address,Nethereum.RPC.Eth.DTOs.BlockParameter.CreateLatest());
        if(request.Exception == null){
            var balance = request.Result.Value;
            callback(Nethereum.Util.UnitConversion.Convert.FromWei(balance, 18));
        }
        else{
            print("Error occured when getting balance");
        }

    }
}
