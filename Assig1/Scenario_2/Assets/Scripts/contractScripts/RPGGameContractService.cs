﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Nethereum.Contracts;
using Nethereum.Hex.HexTypes;
using Nethereum.RPC.Eth.DTOs;


public class RPGGameContractService {

    public string contractAddress = "0x4460E613752c3Bf57adcf6BF08AD81fBcDa63a3A";

    public static string ABI = @"[
	{
		""constant"": false,
		""inputs"": [
			{
				""name"": ""_address"",
				""type"": ""address""
			},
			{
				""name"": ""pumpkins"",
				""type"": ""uint256""
			}
		],
		""name"": ""addPumpkinsToAddress"",
		""outputs"": [],
		""payable"": false,
		""stateMutability"": ""nonpayable"",
		""type"": ""function""
	},
	{
		""constant"": false,
		""inputs"": [],
		""name"": ""buyManaAndReturnChange"",
		""outputs"": [],
		""payable"": true,
		""stateMutability"": ""payable"",
		""type"": ""function""
	},
	{
		""constant"": false,
		""inputs"": [],
		""name"": ""buySeedsAndReturnChange"",
		""outputs"": [],
		""payable"": true,
		""stateMutability"": ""payable"",
		""type"": ""function""
	},
	{
		""constant"": false,
		""inputs"": [
			{
				""name"": ""_address"",
				""type"": ""address""
			},
			{
				""name"": ""seeds"",
				""type"": ""uint256""
			},
			{
				""name"": ""mana"",
				""type"": ""uint256""
			}
		],
		""name"": ""updateManaAndSeedsToAddress"",
		""outputs"": [],
		""payable"": false,
		""stateMutability"": ""nonpayable"",
		""type"": ""function""
	},
	{
		""constant"": true,
		""inputs"": [],
		""name"": ""countInventories"",
		""outputs"": [
			{
				""name"": """",
				""type"": ""uint256""
			}
		],
		""payable"": false,
		""stateMutability"": ""view"",
		""type"": ""function""
	},
	{
		""constant"": true,
		""inputs"": [],
		""name"": ""getAllInventories"",
		""outputs"": [
			{
				""name"": """",
				""type"": ""address[]""
			}
		],
		""payable"": false,
		""stateMutability"": ""view"",
		""type"": ""function""
	},
	{
		""constant"": true,
		""inputs"": [
			{
				""name"": ""addrs"",
				""type"": ""address""
			}
		],
		""name"": ""getManaForAddress"",
		""outputs"": [
			{
				""name"": """",
				""type"": ""uint256""
			}
		],
		""payable"": false,
		""stateMutability"": ""view"",
		""type"": ""function""
	},
	{
		""constant"": true,
		""inputs"": [
			{
				""name"": ""addrs"",
				""type"": ""address""
			}
		],
		""name"": ""getPumpkinsForAddress"",
		""outputs"": [
			{
				""name"": """",
				""type"": ""uint256""
			}
		],
		""payable"": false,
		""stateMutability"": ""view"",
		""type"": ""function""
	},
	{
		""constant"": true,
		""inputs"": [
			{
				""name"": ""addrs"",
				""type"": ""address""
			}
		],
		""name"": ""getSeedsForAddress"",
		""outputs"": [
			{
				""name"": """",
				""type"": ""uint256""
			}
		],
		""payable"": false,
		""stateMutability"": ""view"",
		""type"": ""function""
	}
]";

    private Contract contract;

    public RPGGameContractService(){
        this.contract = new Contract(null, ABI, contractAddress);
    }
	
	#region MANA
    //--------------------------BUY MANA
    public Function GetFunctionBuyMana(){
        return contract.GetFunction("buyManaAndReturnChange");
    }

    public TransactionInput BuyManaTransactionInput(HexBigInteger gas=null, HexBigInteger valueAmount = null){
        var function = GetFunctionBuyMana();
        return function.CreateTransactionInput(LevelManager.objAddress.Address, gas, valueAmount);
    }

    //------------------------GET MANA

    public Function GetFunctionGetManaForAddress(){
        return contract.GetFunction("getManaForAddress");
    }

    public int DecodeManaValue(string result){
        var function = GetFunctionGetManaForAddress();
        return function.DecodeSimpleTypeOutput<int>(result);
    }

    public CallInput CreateManaInput(){
        var function = GetFunctionGetManaForAddress();
        return function.CreateCallInput(LevelManager.objAddress.Address);
    }
	
	//--------------------------UPDATE MANA
    // public Function UpdateFunctionMana(){
    //     return contract.GetFunction("updateManaToAddress");
    // }

    // public TransactionInput UpdateManaTransactionInput(int mana, HexBigInteger gas=null, HexBigInteger valueAmount = null){//int manaAmount = 0, params object[] list
	// 	// Debug.Log("-------------------------------------------------------"+manaAmount);
    //     var function = UpdateFunctionMana();
	// 	//qwerty need to fix here
	// 	// int[] test = new[] {manaAmount};
	// 		// object[] list =  new object[1];
	// 		// list[0] = manaAmount;
	// 		// params object[] parameterList =  new object[1];
	// 		// inputs{
	// 		// 	Mana = manaAmount;
	// 		// }

	// 		// 	""inputs"": [
	// 		// 	{
	// 		// 		""name"": ""_address"",
	// 		// 		""type"": ""address""
	// 		// 	},
	// 		// 	{
	// 		// 		""name"": ""seeds"",
	// 		// 		""type"": ""uint256""
	// 		// 	}
	// 		// ]
	// 		// new HexBigInteger (5);

    //     return function.CreateTransactionInput(LevelManager.objAddress.Address, gas, valueAmount, LevelManager.objAddress.Address, mana);
    // }
	#endregion 
	
	//--------------------------------------------------------------------------------------------------------------------------------------------------------

	#region SEEDS
    //--------------------------BUY SEEDS
    public Function GetFunctionBuySeeds(){
        return contract.GetFunction("buySeedsAndReturnChange");
    }

    public TransactionInput BuySeedsTransactionInput(HexBigInteger gas=null, HexBigInteger valueAmount = null){
        var function = GetFunctionBuySeeds();
        return function.CreateTransactionInput(LevelManager.objAddress.Address, gas, valueAmount);
    }

    //------------------------GET SEEDS

    public Function GetFunctionGetSeedsForAddress(){
        return contract.GetFunction("getSeedsForAddress");
    }

    public int DecodeSeedsValue(string result){
        var function = GetFunctionGetSeedsForAddress();
        return function.DecodeSimpleTypeOutput<int>(result);
    }

    public CallInput CreateSeedsInput(){
        var function = GetFunctionGetSeedsForAddress();
        return function.CreateCallInput(LevelManager.objAddress.Address);
    }

	#endregion

	//--------------------------------------------------------------------------------------------------------------------------------------------------------

	//--------------------------UPDATE MANA AND SEEDS
    public Function UpdateFunctionManaAndSeeds(){
        return contract.GetFunction("updateManaAndSeedsToAddress");
    }

    public TransactionInput UpdateManaAndSeedsTransactionInput(int seeds, int mana, HexBigInteger gas=null,HexBigInteger valueAmount = null){
        var function = UpdateFunctionManaAndSeeds();
        return function.CreateTransactionInput(LevelManager.objAddress.Address, gas, valueAmount, LevelManager.objAddress.Address, seeds, mana);
    }

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------
	#region PUMPKINS
    //--------------------------BUY Pumpkins
    public Function GetFunctionAddPumpkins(){
        return contract.GetFunction("addPumpkinsToAddress");
    }

    public TransactionInput AddPumpkinsTransactionInput(int pumpkins, HexBigInteger gas=null,HexBigInteger valueAmount = null){
        var function = GetFunctionAddPumpkins();
        return function.CreateTransactionInput(LevelManager.objAddress.Address, gas, valueAmount, LevelManager.objAddress.Address, pumpkins);
    }

    //------------------------GET Pumpkins

    public Function GetFunctionGetPumpkinsForAddress(){
        return contract.GetFunction("getPumpkinsForAddress");
    }

    public int DecodePumpkinsValue(string result){
        var function = GetFunctionGetPumpkinsForAddress();
        return function.DecodeSimpleTypeOutput<int>(result);
    }

    public CallInput CreatePumpkinsInput(){
        var function = GetFunctionGetPumpkinsForAddress();
        return function.CreateCallInput(LevelManager.objAddress.Address);
    }
	#endregion

}
