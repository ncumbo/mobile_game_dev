﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Nethereum;

public class LevelManager : MonoBehaviour {
    public static Wallet objAddress;
    public static string _url = "https://rinkeby.infura.io/v3/00fe227c79484a87a30a0dfe55545779";
    public string Password;
    public bool DeleteSharedPrefs = false;

    // public string testPublicAddress = "0x9827469aFF71e9a63305fFeE22A81f0F9c1816ae";
    // public string testPrivateAddress = "F410D34E017FA538A4FBB4AB9F53044F25362576A6CA33E41C91BD20F324AEB9";


	// Use this for initialization
	void Start () {
        //this is done for debugging purposes, developer can delete easily shared prefs for testing
        if(DeleteSharedPrefs == true){
            PlayerPrefs.DeleteAll();
        }

        //------

        objAddress = new Wallet();
        StartCoroutine(CreateOrLoadWallet());
     
	}

    IEnumerator CreateOrLoadWallet(){
        yield return new WaitForFixedUpdate();

        //we try and fetch the address from the shared preferences
        string jsonFromSharedPrefs = PlayerPrefs.GetString("Wallet", "");
        if(jsonFromSharedPrefs == ""){
            //create keys since sharedpreferences is empty
            CreateAccount(Password, (publicAddress, privateKey, privateKeyEncrypted) =>
            {
                //execute the following code after CreateAccount is finished
                // print("Public Address:" + testPublicAddress);
                // print("Private Key:" + testPrivateAddress);
                objAddress.Address = publicAddress;
                objAddress.EncryptedPrivateKey = privateKeyEncrypted;
                System.IO.File.WriteAllText("E:\\Semester1\\Mobile Game Development\\Builds\\Scenario2\\key.txt", "Public Key: "+objAddress.Address+" Private Key Decrypted: "+objAddress.PrivateKey);
                PlayerPrefs.SetString("Wallet", Newtonsoft.Json.JsonConvert.SerializeObject(objAddress));
                PlayerPrefs.Save();
                print("Wallet Saved");
                objAddress.PrivateKey = privateKey;

            });
        }
        else{
            //we need to load wallet if it exists
            print("Loading wallet from shared preferences");
            objAddress = Newtonsoft.Json.JsonConvert.DeserializeObject<Wallet>(jsonFromSharedPrefs);
            objAddress.PrivateKey = Utilities.Decrypt(objAddress.EncryptedPrivateKey, true, Password);
            print("Public Address:" + objAddress.Address);
            print("Private Key Decrypted:" + objAddress.PrivateKey);
                System.IO.File.WriteAllText("E:\\Semester1\\Mobile Game Development\\Builds\\Scenario2\\key.txt", "Public Key: "+objAddress.Address+" Private Key Decrypted: "+objAddress.PrivateKey);

        }

    }
	
	// Update is called once per frame
	void Update () {
		
	}


    private void CreateAccount(string password, System.Action<string, string, string> callback){
        //we going to generate a secure key
        var ecKey = Nethereum.Signer.EthECKey.GenerateKey();

        //let's get the public key generated
        var address = ecKey.GetPublicAddress();

        //getting the private key
        var privateKeyString = ecKey.GetPrivateKey();
        //encrypt the private key so that we can store safely in the shared preferences
        var privateKeyEncrypted = Utilities.Encrypt(privateKeyString, true, Password);
        callback(address, privateKeyString, privateKeyEncrypted); 
    }
}
