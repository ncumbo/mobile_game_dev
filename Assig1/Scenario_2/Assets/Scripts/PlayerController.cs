﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public float speed;
	float edgeXaxis=0f;
	float edgeYaxis=0f;
	public bool moveUp = false;
	public bool moveDown = false;
	public bool moveLeft = false;
	public bool moveRight = false;
	bool mvHoz;
 	bool mvVer;
	public Animator animator;

	private SpriteRenderer spriteRenderer;

	// Use this for initialization
	void Start () {
		edgeXaxis = Camera.main.aspect*Camera.main.orthographicSize;
		edgeYaxis = Camera.main.orthographicSize;
	}
	
	// Update is called once per frame
	void Update () {
		playerMovement();
		setAnimation();
	}

	public void Awake(){
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

    void setAnimation(){
        // animator.SetFloat("Speed", Mathf.Abs(speed));
        animator.SetBool("IsMovingLeft", moveLeft);
        animator.SetBool("IsMovingRight", moveRight);
        animator.SetBool("IsMovingUp", moveUp);
        animator.SetBool("IsMovingDown", moveDown);
    }

	void playerMovement(){
		float xPos = 0, yPos = 0;
		// Debug.Log("Input.GetAxis(Vertical)  "+Input.GetAxis("Vertical") );
		// Debug.Log("Input.GetAxis(Horizontal) "+Input.GetAxis("Horizontal"));
		if (Input.GetAxis("Vertical") != 0 && Input.GetAxis("Horizontal") != 0){
			if (mvHoz){
				yPos = Input.GetAxis("Vertical");
			}else if  (mvVer){
				xPos = Input.GetAxis("Horizontal");
			}
		}else 
		{
			mvHoz = Input.GetAxis("Horizontal") != 0;
			xPos = Input.GetAxis("Horizontal");

			if(xPos != 0){
				if(xPos < 0){	
					moveLeft = true;
					moveUp = false;
					moveDown = false;
					moveRight = false;
					spriteRenderer.flipX = false;
					// Debug.Log("Moving left");
				}else{
					moveRight = true;
					moveUp = false;
					moveDown = false;
					moveLeft = false;
					spriteRenderer.flipX = true;
					// Debug.Log("Moving right");
				}
			}
			mvVer = Input.GetAxis("Vertical") != 0;
			yPos = Input.GetAxis("Vertical");

			if(yPos != 0){
				if(yPos < 0){
					moveDown = true;
					moveUp = false;
					moveRight = false;
					moveLeft = false;
					// Debug.Log("Moving down");
				}else{
					moveUp = true;
					moveDown = false;
					moveRight = false;
					moveLeft = false;
					// Debug.Log("Moving up");
				}
			}

			if(yPos == 0 & xPos == 0){
				moveUp = false;
				moveDown = false;
				moveRight = false;
				moveLeft = false;
			}
		}
		
		transform.position += new Vector3(xPos, yPos, 0).normalized * speed * Time.deltaTime;
		// transform.position = new Vector3(
		// 		Mathf.Clamp(transform.position.x,-edgeXaxis,edgeXaxis),
		// 		Mathf.Clamp(transform.position.y,-edgeYaxis,edgeYaxis));
	}
}
