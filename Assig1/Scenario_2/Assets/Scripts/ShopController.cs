﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopController : MonoBehaviour {

	public GameObject menu;
	public Button btnMana, btnWater, btnSeeds, btnBuy, btnPlant, btnFarm, btnSave;
	private Button selectedButton;
	UiHandler uiEventHandler;
	RPGGameContractCaller contractCaller;
	bool plantGrowingFlag = false, plantExists = false;
	public GameObject plantObject;

	// Use this for initialization
	void Start () {
		menu.SetActive(false);
		plantObject.SetActive(false);

		btnMana.onClick.AddListener(buyMana);
        btnWater.onClick.AddListener(buyWater);
        btnSeeds.onClick.AddListener(buySeeds);
        btnBuy.onClick.AddListener(completePurchase);
		btnPlant.onClick.AddListener(plantSeeds);
		btnFarm.onClick.AddListener(collectPumpkins);
		btnSave.onClick.AddListener(savePumpkins);

		uiEventHandler = GameObject.Find("EventHandler").GetComponent<UiHandler>();
		contractCaller = GameObject.Find("EventHandler").GetComponent<RPGGameContractCaller>();
	}
	
	// Update is called once per frame
	void Update () {
		if(plantExists){
			plantObject.SetActive(true);
		}else{
			plantObject.SetActive(false);
		}
	}

	void buyMana()
    {
        Debug.Log("Mana selected");
		selectedButton = btnMana;
		changeColor(selectedButton);
    }

	void buyWater()
    {
        Debug.Log("Water selected");
		selectedButton = btnWater;
		changeColor(selectedButton);
		uiEventHandler.water +=1;
    }

	void buySeeds()
    {
        Debug.Log("Seeds selected");
		selectedButton = btnSeeds;
		changeColor(selectedButton);
    }

	void plantSeeds()
    {
        Debug.Log("plant seeds selected");
		//selectedButton = btnPlant;
		// changeColor(btnPlant);
		uiEventHandler.mana -= 1;
		uiEventHandler.water -= 1;
		uiEventHandler.seeds -= 1;

		contractCaller.updateManaAndSeedsForAddress();

		plantGrowingFlag = true;
		StartCoroutine(showPumpkin());
		btnPlant.interactable = false;
    }

	IEnumerator showPumpkin()
 	{
    	yield return new WaitForSeconds(5f);
		plantExists = true;
		btnFarm.interactable = true;
 	}

	void collectPumpkins()
    {
        Debug.Log("collect pumpkins selected");
		// selectedButton = btnFarm;
		// changeColor(btnFarm);
		plantGrowingFlag = false;
		plantExists = false;
		uiEventHandler.pumpkins += 1;
		btnFarm.interactable = false;
    }

	void savePumpkins()
    {
        Debug.Log("save pumpkins selected");
		// selectedButton = btnSave;
		// changeColor(btnSave);
		contractCaller.addPumpkins();
    }

	void completePurchase()
    {
		if(selectedButton){
			Debug.Log("bought Item: "+selectedButton.name);
			// menu.SetActive(false);
			switch(selectedButton.name){
				case "btnMana":
					contractCaller.buyMana(); 
				break;
				case "btnSeeds":
					contractCaller.buySeeds(); 
				break;
			}
		}else{
			Debug.Log("No item selected");
		}

    }

	void OnTriggerEnter2D(Collider2D col)
    {
		// print("Shop Enter "+col.gameObject.name);
        if(col.GetType() == typeof(BoxCollider2D)){
			switch(col.gameObject.name){
				case "SaveArea":
					menu.SetActive(true);
					btnMana.interactable = false;
					btnWater.interactable = false;
					btnSeeds.interactable = false;
					btnPlant.interactable = false;
					btnFarm.interactable = false;
					btnSave.interactable = true;
					btnBuy.interactable = false;
				break;
				case "OnlineShop":
					menu.SetActive(true);
					btnMana.interactable = true;
					btnWater.interactable = false;
					btnSeeds.interactable = true;
					btnPlant.interactable = false;
					btnFarm.interactable = false;
					btnSave.interactable = false;
					btnBuy.interactable = true;
				break;
				case "Water":
					menu.SetActive(true);
					btnMana.interactable = false;
					btnWater.interactable = true;
					btnSeeds.interactable = false;
					btnPlant.interactable = false;
					btnFarm.interactable = false;
					btnSave.interactable = false;
					btnBuy.interactable = false;
				break;
				case "PumpkinField":
					menu.SetActive(true);
					btnMana.interactable = false;
					btnWater.interactable = false;
					btnSeeds.interactable = false;
					btnSave.interactable = false;
					btnBuy.interactable = false;
					//here we need to check if plant exists
					int mana = uiEventHandler.mana;
					int water = uiEventHandler.water;
					int seeds = uiEventHandler.seeds;
					if(mana>0 & water>0 & seeds>0 & plantGrowingFlag == false & plantExists == false){
						btnPlant.interactable = true;
					}
					else{
						btnPlant.interactable = false;
					}

					if(plantGrowingFlag == true & plantExists == true){
						btnFarm.interactable = true;
					}
					else{
						btnFarm.interactable = false;
					}
					
				break;
			}
		}
    }
	void OnTriggerExit2D(Collider2D col)
    {
        // print("Shop Exit");
        if(col.GetType() == typeof(BoxCollider2D)){
			switch(col.gameObject.name){
				case "SaveArea":
					menu.SetActive(false);
				break;
				case "OnlineShop":
					menu.SetActive(false);
				break;
				case "Water":
					menu.SetActive(false);
				break;
				case "PumpkinField":
					menu.SetActive(false);
				break;
			}
		}
    }

	public void changeColor(Button selectedButton) {
		//https://answers.unity.com/questions/1274141/how-can-i-change-the-highlighted-color-of-a-unity.html
		// Debug.Log ("Changing highlighed color");
		float r = Random.Range (0f, 1f);
		float g = Random.Range (0f, 1f);
		float b = Random.Range (0f, 1f);
		ColorBlock colorVar = selectedButton.colors;
		colorVar.highlightedColor = new Color (r, g, b);
		selectedButton.colors = colorVar;
    }
}
