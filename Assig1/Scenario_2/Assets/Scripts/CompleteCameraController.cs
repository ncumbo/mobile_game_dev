﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompleteCameraController : MonoBehaviour {

    public GameObject player;
    private Vector3 offset; //store the offset distance between the player and the camera

	// Use this for initialization
	void Start () {

      //  offset = transform.position - player.transform.position;
        this.AssignPlayer(player);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void AssignPlayer(GameObject player){
        //calculate and store the offset value by getting the distance between the player's position
        //and camera's position
        this.player = player;
        offset = transform.position - this.player.transform.position;
    }

    //LateUpdate is called after Update each frame
    void LateUpdate()
    {
        if(player != null){
            //set the position of the camera's transform  to be the same of the player
            Vector3 tempPosition = player.transform.position;
            //tempPosition.y = tempPosition.y + 3f;
            transform.position = tempPosition + offset;
        }
   
    }


}
