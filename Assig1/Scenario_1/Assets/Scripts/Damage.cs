﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour {

    private float damageTimeout = 0.5f;
    private bool waterFlag = false;
    private bool damageFlag = true;
    public GameObject player;

    void OnCollisionEnter2D(Collision2D collision) {
        // print("Collision");
        if(damageFlag && collision.gameObject.tag == "Player") {
            // NetworkLayer.TakeDamage(1);
            GameObject.Find("Scripts").GetComponent<LevelManager>().TakeDamage();
            StartCoroutine(damageTimer());
        }
    }

    void OnTriggerEnter2D(Collider2D collision){
        if(!waterFlag && collision.gameObject.tag == "Player") {
            player = collision.gameObject;
            // print(this.gameObject.name);
            waterFlag = true;//player is in water
            // NetworkLayer.TakeDamage(1);
            GameObject.Find("Scripts").GetComponent<LevelManager>().TakeDamage();//take damage
            StartCoroutine(respawnPlayer());
        }
    }

    private IEnumerator damageTimer() {
        damageFlag = false;
        yield return new WaitForSeconds(damageTimeout);
        damageFlag = true;
    }

    private IEnumerator respawnPlayer() {
        yield return new WaitForSeconds(2f);
        player.transform.position = new Vector2(0, 0);
        waterFlag = false;
    }

}
