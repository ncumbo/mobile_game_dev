﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {
    private int score;
    public int life;
    public Text scoreText;
    public Text lifeText;
    public float timeLeft = 30;
    public GameObject player;
    CompleteCameraController cameraController;

	// Use this for initialization
	void Start () {
        cameraController = GameObject.Find("Main Camera").GetComponent<CompleteCameraController>();
	}
	
	// Update is called once per frame
	void Update () {
		CheckLifes();
	}

    public void IncreaseScore(int value){
        score = score + value;
        scoreText.text = "Score: " + score.ToString();
    }

    public void TakeDamage(){
        // Debug.Log(life);
        life --;
        lifeText.text = "Life: " + life.ToString();
    }

    private void CheckLifes(){
        // Debug.Log(life);
        if(life == 0){
            // SceneManager.LoadScene("GameOver");
            StartCoroutine(respawnPlayerGameOver());
        }
    }

    private IEnumerator respawnPlayerGameOver() {
        yield return new WaitForSeconds(2f);
        cameraController.player.transform.position = new Vector2(0, -3.86f);
        cameraController.player.GetComponent<PlayerMovement>().enabled = false;
    }

}
