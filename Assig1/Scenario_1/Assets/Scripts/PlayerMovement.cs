using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : Photon.MonoBehaviour {

	public CharacterController2D controller;
    public Joystick joystick;

	public float runSpeed = 40f;

	public float horizontalMove = 0f;
	public bool jump = false;
	// public bool crouch = false;

    public bool testForMobile = false;
    public Animator animator;

    void Start()
    {
        if(photonView.isMine){
            joystick = GameObject.FindWithTag("Fixed Joystick").GetComponent<FixedJoystick>();
        }
    }

    // Update is called once per frame
    void Update () {

        if(photonView.isMine){
            //print(joystick.Horizontal);
            if (testForMobile == true)
            {
                MobileMovement();
            }
            else
            {
                UnityEditorMovement();
            }
        }
   

        setAnimation();
	}

    void MobileMovement(){
        horizontalMove = joystick.Horizontal * runSpeed;
        float verticalMove = joystick.Vertical;

        if(verticalMove > 0.5){
            jump = true;
        }
        else{
            jump = false;
        }
    }

    void UnityEditorMovement(){
        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;

        if (Input.GetButtonDown("Jump"))
        {
            jump = true;
        }
    }

	void FixedUpdate ()
	{
        if(photonView.isMine){
            // Move our character
            controller.Move(horizontalMove * Time.fixedDeltaTime, jump);
            jump = false;
        }
		
	}

    void setAnimation(){
        animator.SetFloat("Speed", Mathf.Abs(horizontalMove));
        animator.SetBool("IsJumping", jump);
    }
}
