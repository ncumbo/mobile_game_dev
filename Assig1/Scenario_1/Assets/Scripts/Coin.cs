﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D collision)
    {
        // print("Collision");
        if(collision.GetType() == typeof(BoxCollider2D)){
            if(collision.gameObject.tag == "Player"){
                NetworkLayer.CollectCoin(this.gameObject.name, collision.gameObject.name);
                GameObject.Find("Scripts").GetComponent<LevelManager>().IncreaseScore(1);
            }
        }
    }
}
