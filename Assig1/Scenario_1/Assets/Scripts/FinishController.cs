﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishController : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D collision) {
		// Debug.Log(collision.gameObject.name);
		if(collision.GetType() == typeof(BoxCollider2D)){
			if(collision.gameObject.tag == "Player") {
				Debug.Log("Game Completed ");
				NetworkLayer.finishGame(collision.gameObject.name);
			}
		}
    }
}
