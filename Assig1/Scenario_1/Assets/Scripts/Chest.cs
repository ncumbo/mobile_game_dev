﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour {
	public bool chestFlag = false;
    public Animator animator;
    void OnTriggerEnter2D(Collider2D collision)
    {
        // print("collect Chest");
        if(collision.GetType() == typeof(BoxCollider2D)){
            if(collision.gameObject.tag == "Player"){
				if(chestFlag == false){
                    chestFlag = true;
                    NetworkLayer.CollectChest(this.gameObject.name, collision.gameObject.name);
                    GameObject.Find("Scripts").GetComponent<LevelManager>().IncreaseScore(5);
				}
                else{print("chest already collected");}
            }
        }
    }

    void Update(){
        // Debug.Log(chestFlag);
        if(chestFlag == true){
            setAnimation();
        }
    }

    void setAnimation(){
        animator.SetBool("isOpen",true);
    }
}
