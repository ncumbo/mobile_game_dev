﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Photon.MonoBehaviour {

    private Vector3 correctPlayerPos;
    private Vector3 correctPlayerScale;

    private float correctPlayerHorizontalMove;
    private bool correctPlayerJump;
    // private bool correctPlayerCrouch;

	// Use this for initialization
	void Start () {
        if(!photonView.isMine){
            //if player object is not mine, then it should automatically be controlled by photon data
            //therefore destroy rigidbody
            Destroy(GetComponent<Rigidbody2D>());
            Destroy(GetComponent<CharacterController2D>());
            Destroy(GetComponent<BoxCollider2D>());
            Destroy(GetComponent<CircleCollider2D>());
        }
	}
	
	// Update is called once per frame
	void Update () {
        if(!photonView.isMine){
            //if player object is not mine, then we need to manually change its position with
            //data from server

            //Lerp is used to create a smooth animation
            transform.position = Vector3.Lerp(transform.position, 
                                              this.correctPlayerPos, Time.deltaTime * 10);

            transform.localScale = new Vector3(correctPlayerScale.x, correctPlayerScale.y,
                                               correctPlayerScale.z);

            GetComponent<PlayerMovement>().horizontalMove = correctPlayerHorizontalMove;
            GetComponent<PlayerMovement>().jump = correctPlayerJump;
            // GetComponent<PlayerMovement>().crouch = correctPlayerCrouch;


        }
	}

    //This method is auto called regularly by PhotonCloud
    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info){
        if(stream.isWriting){
            //we own this instance (this player), therefore send others our data
            stream.SendNext(transform.position);
            stream.SendNext(transform.localScale);
            stream.SendNext(GetComponent<PlayerMovement>().horizontalMove);
            stream.SendNext(GetComponent<PlayerMovement>().jump);
            // stream.SendNext(GetComponent<PlayerMovement>().crouch);
        }
        else{
            //receive data from other player
            this.correctPlayerPos = (Vector3)stream.ReceiveNext();
            this.correctPlayerScale = (Vector3)stream.ReceiveNext();
            this.correctPlayerHorizontalMove = (float)stream.ReceiveNext();
            this.correctPlayerJump = (bool)stream.ReceiveNext();
            // this.correctPlayerCrouch = (bool)stream.ReceiveNext();
        }


    }
}
