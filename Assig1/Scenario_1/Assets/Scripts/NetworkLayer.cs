﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NetworkLayer : MonoBehaviour {

    private static PhotonView ScriptsPhotonView;
    NetworkManager networkManager;
    // public Animator chestAnimation; 

	// Use this for initialization
	void Start () {
        ScriptsPhotonView = this.GetComponent<PhotonView>();
        networkManager = GameObject.Find("Scripts").GetComponent<NetworkManager>();
	}

    public void SpawnCrateAuto(){
        //spawn crate after 5 seconds
        StartCoroutine(SpawnCrate());

    }

    IEnumerator SpawnCrate(){
        yield return new WaitForSeconds(5f);
        SpawnCrateToEveryone();
    }

    private void SpawnCrateToEveryone(){
        //send message to all connected players (event master client) with random
        //coordinates of crate
        float x = Random.Range(18.0f, 25.0f);
        ScriptsPhotonView.RPC("SpawnCrateRPC", PhotonTargets.AllBuffered, new float[] { x, 0.5f, 0 });
    }

    [PunRPC]
    void SpawnCrateRPC(float[] coordinates){
        var crate = Resources.Load<GameObject>("crate");
        Instantiate(crate, new Vector3(coordinates[0],coordinates[1], coordinates[2]), Quaternion.identity);
        print("Crate created");
    }

    public static void CollectCoin(string cherryName, string playerName){
        //send message to all players that cherry has to be collected
        ScriptsPhotonView.RPC("CollectCoinRPC", PhotonTargets.All, cherryName, playerName);
    }

    [PunRPC]
    void CollectCoinRPC(string cherryName, string playerName){
        GameObject.Destroy(GameObject.Find(cherryName));
        if(playerName == "Player1"){
            networkManager.player1Score += 1;
        }else if (playerName == "Player2"){
            networkManager.player2Score += 1;
        }
    }

    public static void CollectChest(string chestName, string playerName){
        ScriptsPhotonView.RPC("CollectChestRPC", PhotonTargets.All, chestName, playerName);
    }

    [PunRPC]
    void CollectChestRPC(string chestName, string playerName){
        GameObject chest= GameObject.Find(chestName);
        chest.GetComponent<Chest>().chestFlag = true;
        if(playerName == "Player1"){
            networkManager.player1Score += 5;
        }else if (playerName == "Player2"){
            networkManager.player2Score += 5;
        }
    }

    void OnPhotonSerializeView(PhotonStream stream,PhotonMessageInfo info){

    }

    
    public static void finishGame(string winner){
        //send message to all players that cherry has to be collected
        ScriptsPhotonView.RPC("FinishGameRPC", PhotonTargets.All, winner);
    }

    [PunRPC]
    void FinishGameRPC(string winner){
       networkManager.gameEnded = true;
       networkManager.winnerName = winner;
    }

}
