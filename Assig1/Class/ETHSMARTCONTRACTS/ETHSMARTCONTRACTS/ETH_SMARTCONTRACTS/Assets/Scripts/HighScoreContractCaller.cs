﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Nethereum.JsonRpc.UnityClient;
using Nethereum.Hex.HexTypes;
using Nethereum.Contracts;


public class HighScoreContractCaller : MonoBehaviour {

    private DeployContractTransactionBuilder contractTransactionBuilder = 
        new DeployContractTransactionBuilder();

    private HighScoreContractService scoreTokenContractService 
                = new HighScoreContractService();

	// Use this for initialization
	void Start () {
        //toggle between checkHighScore and setHighScore
        //setHighScore("Richard", 2000);
        checkHighScore();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //------------TO READ DATA---------------------
    public void checkHighScore(){
        StartCoroutine(getHighScoreRequest());
    }

    public IEnumerator getHighScoreRequest(){
        var getScoreRequest = new EthCallUnityRequest(LevelManager._url);
        var getScoreInput = scoreTokenContractService.CreateScoreInput();
        print("Getting Scores");
        yield return getScoreRequest.SendRequest(getScoreInput,
                                                 Nethereum.RPC.Eth.DTOs.BlockParameter.CreateLatest());
        if(getScoreRequest.Exception == null){
            HighestScoreDTO objScore = scoreTokenContractService.DecodeTopScore(getScoreRequest.Result);
            print("Username:" + objScore.username);
            print("Score:" + objScore.score);
        }
        else{
            print("Error getting scores:" + getScoreRequest.Exception.Message);
        }
    }

    //------------ TO WRITE DATA----------------------
    public void setHighScore(string username, int score){
        StartCoroutine(setHighScoreRequest(username, score));
    }

    public IEnumerator setHighScoreRequest(string username, int score){
        yield return new WaitForSeconds(3); //this is done only for testing purposes because we are going
        //to call this method from the Start Method

        var transactionInput = scoreTokenContractService
                         .CreateSetTopScoreTransactionInput(username, score, new HexBigInteger(3000000));

        var transactionSignedRequest = 
            new TransactionSignedUnityRequest(LevelManager._url,
                         LevelManager.objAddress.PrivateKey, LevelManager.objAddress.Address);

        //send and wait
        yield return transactionSignedRequest.SignAndSendTransaction(transactionInput);

        if(transactionSignedRequest.Exception == null){
            print(transactionSignedRequest.Result); //transaction address
        }
        else
        {
            print("Error saving scores:" + transactionSignedRequest.Exception.Message);
        }

    }


}
