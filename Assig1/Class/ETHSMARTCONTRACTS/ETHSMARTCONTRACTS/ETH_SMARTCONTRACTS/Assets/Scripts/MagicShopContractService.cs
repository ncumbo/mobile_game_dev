﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Nethereum.Contracts;
using Nethereum.Hex.HexTypes;
using Nethereum.RPC.Eth.DTOs;


public class MagicShopContractService {

    public string contractAddress = "0x7a27e54F1E13CD79661EE13Ad5326e0b0F2Bd87d";

    public static string ABI = @"[
    {
        ""constant"": false,
        ""inputs"": [
            {
                ""name"": ""_address"",
                ""type"": ""address""

            }
        ],
        ""name"": ""addManaToaddress"",
        ""outputs"": [],
        ""payable"": false,
        ""stateMutability"": ""nonpayable"",
        ""type"": ""function""
    },
    {
        ""constant"": false,
        ""inputs"": [],
        ""name"": ""buyManaAndReturnChange"",
        ""outputs"": [],
        ""payable"": true,
        ""stateMutability"": ""payable"",
        ""type"": ""function""
    },
    {
        ""constant"": true,
        ""inputs"": [],
        ""name"": ""countStores"",
        ""outputs"": [
            {
                ""name"": """",
                ""type"": ""uint256""
            }
        ],
        ""payable"": false,
        ""stateMutability"": ""view"",
        ""type"": ""function""
    },
    {
        ""constant"": true,
        ""inputs"": [
            {
                ""name"": ""_address"",
                ""type"": ""address""
            }
        ],
        ""name"": ""getManaForAddress"",
        ""outputs"": [
            {
                ""name"": """",
                ""type"": ""uint256""
            }
        ],
        ""payable"": false,
        ""stateMutability"": ""view"",
        ""type"": ""function""
    },
    {
        ""constant"": true,
        ""inputs"": [],
        ""name"": ""getStores"",
        ""outputs"": [
            {
                ""name"": """",
                ""type"": ""address[]""
            }
        ],
        ""payable"": false,
        ""stateMutability"": ""view"",
        ""type"": ""function""
    }
]";

    private Contract contract;

    public MagicShopContractService(){
        this.contract = new Contract(null, ABI, contractAddress);
    }

    //--------------------------BUY MANA
    public Function GetFunctionBuyMana(){
        return contract.GetFunction("buyManaAndReturnChange");
    }

    public TransactionInput BuyManaTransactionInput(HexBigInteger gas=null,HexBigInteger valueAmount = null){
        var function = GetFunctionBuyMana();
        return function.CreateTransactionInput(LevelManager.objAddress.Address, gas, valueAmount);
    }

    //------------------------GET MANA

    public Function GetFunctionGetManaForAddress(){
        return contract.GetFunction("getManaForAddress");
    }

    public int DecodeManaValue(string result){
        var function = GetFunctionGetManaForAddress();
        return function.DecodeSimpleTypeOutput<int>(result);
    }

    public CallInput CreateManaInput(){
        var function = GetFunctionGetManaForAddress();
        return function.CreateCallInput(LevelManager.objAddress.Address);
    }



}
