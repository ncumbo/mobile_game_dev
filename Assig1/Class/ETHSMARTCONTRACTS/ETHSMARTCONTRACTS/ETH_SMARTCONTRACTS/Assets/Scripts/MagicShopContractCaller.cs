﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Nethereum.JsonRpc.UnityClient;
using Nethereum.Hex.HexTypes;
using Nethereum.Contracts;

public class MagicShopContractCaller : MonoBehaviour {

    private DeployContractTransactionBuilder contractTransactionBuilder
                                            = new DeployContractTransactionBuilder();

    private MagicShopContractService magicShopContractService
                                 = new MagicShopContractService();
                        


    void buyMana(){
        StartCoroutine(buyManaRequest());
    }

    public IEnumerator buyManaRequest(){
        //this delay can be deleted. only done since we are going to call from start
        yield return new WaitForSeconds(3);
        var transactionInput = magicShopContractService.BuyManaTransactionInput(new HexBigInteger(3000000)
                                                                              , new HexBigInteger(1000));

        var transactionSignedRequest = new TransactionSignedUnityRequest(LevelManager._url,
                                                                        LevelManager.objAddress.PrivateKey,
                                                                         LevelManager.objAddress.Address);

        //send and wait (run smart contract)
        yield return transactionSignedRequest.SignAndSendTransaction(transactionInput);
        if(transactionSignedRequest.Exception == null){
            print("Transaction receipt:" + transactionSignedRequest.Result);//transaction address

            while(true){ //every 3 seconds check for transaction feedback until received
                yield return new WaitForSeconds(3);
                EthGetTransactionReceiptUnityRequest tranReceiptStatus =
                    new EthGetTransactionReceiptUnityRequest(LevelManager._url);
                yield return tranReceiptStatus.SendRequest(transactionSignedRequest.Result);
                if(tranReceiptStatus.Result != null){
                    if(tranReceiptStatus.Result.Status != null){
                        if(tranReceiptStatus.Result.Status.Value == 1){//smart contract was successfully executed
                            print("Purchased Mana Successfully");
                        }
                        else if(tranReceiptStatus.Result.Status.Value == 0){
                            print("Error when buying mana");
                        }
                        break;
                    }
                }

            }

        }
        else{
            print("Error when buying mana:" + transactionSignedRequest.Exception.Message);
        }

    }

    void getManaForAddress(){
        StartCoroutine(getManaRequestForAddress());
    }

    IEnumerator getManaRequestForAddress(){
        yield return new WaitForSeconds(3);
        var getManaRequest = new EthCallUnityRequest(LevelManager._url);
        var getManaInput = magicShopContractService.CreateManaInput();
        print("Loading mana for account:" + LevelManager.objAddress.Address);
        yield return getManaRequest.SendRequest(getManaInput,
                                                Nethereum.RPC.Eth.DTOs.BlockParameter.CreateLatest());
        if(getManaRequest.Exception == null){
            int mana = magicShopContractService.DecodeManaValue(getManaRequest.Result);
            print("Mana:" + mana);
        }
        else{
            print("Error getting mana:" + getManaRequest.Exception.Message);
        }

    }


	// Use this for initialization
	void Start () {
        //toggle between these two methods for testing
       // buyMana();
        getManaForAddress();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
