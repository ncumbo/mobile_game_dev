﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Nethereum.RPC.Eth.DTOs;
using Nethereum.ABI.FunctionEncoding.Attributes;
using Nethereum.Contracts;
using Nethereum.Hex.HexTypes;


public class HighScoreContractService {

    public string contractAdress = "0x2D5Df1A5D8AC3b204E41C97c469a49c9A69017c9";

    //ABI OF CONTRACT. (DEPLOYED USING REMIX IDE)

    public static string ABI = @"[
    {
        ""constant"": true,
        ""inputs"": [],
        ""name"": ""username"",
        ""outputs"": [
            {
                ""name"": """",
                ""type"": ""string""

            }
        ],
        ""payable"": false,
        ""stateMutability"": ""view"",
        ""type"": ""function""
    },
    {
        ""constant"": true,
        ""inputs"": [
            {
                ""name"": """",
                ""type"": ""uint256""
            }
        ],
        ""name"": ""arrScores"",
        ""outputs"": [
            {
                ""name"": ""username"",
                ""type"": ""string""
            },
            {
                ""name"": ""score"",
                ""type"": ""int256""
            }
        ],
        ""payable"": false,
        ""stateMutability"": ""view"",
        ""type"": ""function""
    },
    {
        ""constant"": false,
        ""inputs"": [
            {
                ""name"": ""username_value"",
                ""type"": ""string""
            },
            {
                ""name"": ""score_value"",
                ""type"": ""int256""
            }
        ],
        ""name"": ""setScore"",
        ""outputs"": [],
        ""payable"": false,
        ""stateMutability"": ""nonpayable"",
        ""type"": ""function""
    },
    {
        ""constant"": true,
        ""inputs"": [],
        ""name"": ""score"",
        ""outputs"": [
            {
                ""name"": """",
                ""type"": ""int256""
            }
        ],
        ""payable"": false,
        ""stateMutability"": ""view"",
        ""type"": ""function""
    }
]";

    private Contract contract;

    public HighScoreContractService(){
        this.contract = new Contract(null, ABI, contractAdress);
    }

    //---------------------------READ SCORE
    public Function GetFunctionHighestScore(){
        return contract.GetFunction("arrScores");
    }

    public HighestScoreDTO DecodeTopScore(string result){
        var function = GetFunctionHighestScore();
        return function.DecodeDTOTypeOutput <HighestScoreDTO> (result) ;
    }

    public CallInput CreateScoreInput(){
        var function = GetFunctionHighestScore();
        return function.CreateCallInput();
    }

    //---------------------------WRITE SCORE
    public Function GetFunctionSetTopScore(){
        return contract.GetFunction("setScore");
    }

    public TransactionInput CreateSetTopScoreTransactionInput(string username_value, int score_value,
                                                              HexBigInteger gas=null,
                                                              HexBigInteger valueAmount=null){


        var function = GetFunctionSetTopScore();
        return function.CreateTransactionInput(LevelManager.objAddress.Address, gas,
                                               valueAmount, username_value, score_value);
    }





}

[FunctionOutput]
public class HighestScoreDTO{
    [Parameter("string", "username", 1)]
    public string username { get; set; }

    [Parameter("int256", "score", 2)]
    public int score { get; set; }
}
