﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Nethereum.JsonRpc.UnityClient;

public class BalanceCheck : MonoBehaviour {

	// Use this for initialization
	void Start () {
        getBalance();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void getBalance(){
        StartCoroutine(getEthInAccount((balance) =>
        {
            print("Balance in account:" + balance);

        }));
    }

    public IEnumerator getEthInAccount(System.Action<decimal> callback){
        yield return new WaitForSeconds(3); //this delay is done so that we give time for wallet to load
        var request = new EthGetBalanceUnityRequest(LevelManager._url);
        yield return request.SendRequest(LevelManager.objAddress.Address,
                                         Nethereum.RPC.Eth.DTOs.BlockParameter.CreateLatest());
        if(request.Exception == null){
            var balance = request.Result.Value;
            callback(Nethereum.Util.UnitConversion.Convert.FromWei(balance, 18));
        }
        else{
            print("Error occured when getting balance");
        }

    }
}
