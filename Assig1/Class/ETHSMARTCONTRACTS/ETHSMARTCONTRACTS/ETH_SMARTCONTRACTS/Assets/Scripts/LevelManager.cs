﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Nethereum;

public class LevelManager : MonoBehaviour {
    public static Wallet objAddress;
    public static string _url = "https://rinkeby.infura.io/v3/cf20ab8f01824d2db9d4981f70d33828";
    public string Password;
    public bool DeleteSharedPrefs = false;


	// Use this for initialization
	void Start () {
        //this is done for debugging purposes, developer can delete easily shared prefs for testing
        if(DeleteSharedPrefs == true){
            PlayerPrefs.DeleteAll();
        }

        //------

        objAddress = new Wallet();
        StartCoroutine(CreateOrLoadWallet());
     
	}

    IEnumerator CreateOrLoadWallet(){
        yield return new WaitForFixedUpdate();

        //we try and fetch the address from the shared preferences
        string jsonFromSharedPrefs = PlayerPrefs.GetString("Wallet", "");
        if(jsonFromSharedPrefs == ""){
            //create keys since sharedpreferences is empty
            CreateAccount(Password, (publicAddress, privateKey, privateKeyEncrypted) =>
            {
                //execute the following code after CreateAccount is finished
                print("Public Address:" + publicAddress);
                print("Private Key:" + privateKey);
                objAddress.Address = publicAddress;
                objAddress.EncryptedPrivateKey = privateKeyEncrypted;

                PlayerPrefs.SetString("Wallet", Newtonsoft.Json.JsonConvert.SerializeObject(objAddress));
                PlayerPrefs.Save();
                print("Wallet Saved");
                objAddress.PrivateKey = privateKey;

            });
        }
        else{
            //we need to load wallet if it exists
            print("Loading wallet from shared preferences");
            objAddress = Newtonsoft.Json.JsonConvert.DeserializeObject<Wallet>(jsonFromSharedPrefs);
            objAddress.PrivateKey = Utilities.Decrypt(objAddress.EncryptedPrivateKey, true, Password);
            print("Public Address:" + objAddress.Address);
            print("Private Key Decrypted:" + objAddress.PrivateKey);
        }

    }
	
	// Update is called once per frame
	void Update () {
		
	}


    private void CreateAccount(string password, System.Action<string, string, string> callback){
        //we going to generate a secure key
        var ecKey = Nethereum.Signer.EthECKey.GenerateKey();

        //let's get the public key generated
        var address = ecKey.GetPublicAddress();

        //getting the private key
        var privateKeyString = ecKey.GetPrivateKey();
        //encrypt the private key so that we can store safely in the shared preferences
        var privateKeyEncrypted = Utilities.Encrypt(privateKeyString, true, Password);
        callback(address, privateKeyString, privateKeyEncrypted); 
    }
}
